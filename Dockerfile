FROM nginx
ENV NGINX_PORT=8080
COPY ./templates/default.conf /etc/nginx/conf.d/default.conf
COPY ./html /usr/share/nginx/html
